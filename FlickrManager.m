//
//  FlickrManager.m
//  Photo Styling
//
//  Created by Roman Kopaliani on 7/27/12.
//  Copyright (c) 2012 FactorialComplexity. All rights reserved.
//

#import "FlickrManager.h"

NSString *kStoredAuthTokenKeyName = @"FlickrOAuthToken";
NSString *kStoredAuthTokenSecretKeyName = @"FlickrOAuthTokenSecret";

NSString *kGetAccessTokenStep = @"kGetAccessTokenStep";
NSString *kCheckTokenStep = @"kCheckTokenStep";
NSString *kFetchRequestTokenStep = @"kFetchRequestTokenStep";
NSString *kGetUserInfoStep = @"kGetUserInfoStep";
NSString *kSetImagePropertiesStep = @"kSetImagePropertiesStep";
NSString *kUploadImageStep = @"kUploadImageStep";


NSString *CallbackURLBaseString = @"instaframedeluxe://auth";

@interface FlickrManager () <OFFlickrAPIRequestDelegate>

@property (nonatomic, strong) OFFlickrAPIContext *flickrContext;
@property (nonatomic, strong) OFFlickrAPIRequest *flickrRequest;
@property (nonatomic, strong) NSString *flickrUserName;
@property (nonatomic, strong) UIImage *image;

@end

@implementation FlickrManager
@synthesize flickrContext = _flickrContext, flickrRequest  =_flickrRequest;
@synthesize flickrUserName  =_flickrUserName;
@synthesize image = _image;
@synthesize delegate = _delegate;

- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

+ (FlickrManager *)sharedManager
{
    static dispatch_once_t pred;
    static FlickrManager *sharedManager = nil;
    
    dispatch_once(&pred, ^{ sharedManager = [[self alloc] init]; });
    return sharedManager;
}


#pragma mark - Publilc

- (BOOL)flickrHandleURL:(NSURL *)anURL
{
    NSString *token = nil;
    NSString *verifier = nil;
    BOOL result = OFExtractOAuthCallback(anURL, [NSURL URLWithString:CallbackURLBaseString], &token, &verifier);
    
    if (!result) {
        NSLog(@"Cannot obtain token/secret from URL: %@", [anURL absoluteString]);
        return NO;
    }
    
    [self flickrRequest].sessionInfo = kGetAccessTokenStep;
    [self.flickrRequest fetchOAuthAccessTokenWithRequestToken:token verifier:verifier];
    return YES;
}

- (void)uploadImage:(UIImage *)image
{
    self.image = image;
    if ([self.flickrContext.OAuthToken length]) {
		[self flickrRequest].sessionInfo = kCheckTokenStep;
        [self startUpload:self.image];
    }
    else {
        [self authenticate];
    }
}

- (BOOL)isFlickrLoggedIn
{
    return [self.flickrContext.OAuthToken length];
}

#pragma mark - Lazy getters

- (OFFlickrAPIContext *)flickrContext
{
    if (!_flickrContext) {
        _flickrContext = [[OFFlickrAPIContext alloc] initWithAPIKey:@"a423eaec4afd70e5e9fe7a2ba2a24a03"
                                                       sharedSecret:@"ddcecd743acdcdf4"];
        
        NSString *authToken = [[NSUserDefaults standardUserDefaults] objectForKey:kStoredAuthTokenKeyName];
        NSString *authTokenSecret = [[NSUserDefaults standardUserDefaults] objectForKey:kStoredAuthTokenSecretKeyName];
        
        if (([authToken length] > 0) && ([authTokenSecret length] > 0)) {
            _flickrContext.OAuthToken = authToken;
            _flickrContext.OAuthTokenSecret = authTokenSecret;
        }
    }
    return _flickrContext;
}

- (OFFlickrAPIRequest *)flickrRequest
{
	if (!_flickrRequest) {
		_flickrRequest = [[OFFlickrAPIRequest alloc] initWithAPIContext:self.flickrContext];
		_flickrRequest.delegate = self;
	}
	
	return _flickrRequest;
}

#pragma mark - Private

- (void)setAndStoreFlickrAuthToken:(NSString *)inAuthToken secret:(NSString *)inSecret
{
	if (![inAuthToken length] || ![inSecret length]) {
		self.flickrContext.OAuthToken = nil;
        self.flickrContext.OAuthTokenSecret = nil;
		[[NSUserDefaults standardUserDefaults] removeObjectForKey:kStoredAuthTokenKeyName];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kStoredAuthTokenSecretKeyName];
        
	}
	else {
		self.flickrContext.OAuthToken = inAuthToken;
        self.flickrContext.OAuthTokenSecret = inSecret;
		[[NSUserDefaults standardUserDefaults] setObject:inAuthToken forKey:kStoredAuthTokenKeyName];
		[[NSUserDefaults standardUserDefaults] setObject:inSecret forKey:kStoredAuthTokenSecretKeyName];
	}
}

- (void)cancelAction
{
	[_flickrRequest cancel];
	[self setAndStoreFlickrAuthToken:nil secret:nil];
}

- (void)authenticate
{
    self.flickrRequest.sessionInfo = kFetchRequestTokenStep;
    [self.flickrRequest fetchOAuthRequestTokenWithCallbackURL:[NSURL URLWithString:CallbackURLBaseString]];
}

- (void)startUpload:(UIImage *)image
{
    NSData *JPEGData = UIImageJPEGRepresentation(image, 1.0);
    self.flickrRequest.sessionInfo = kUploadImageStep;
    [self.flickrRequest uploadImageStream:[NSInputStream inputStreamWithData:JPEGData]
                        suggestedFilename:@"Split Foto - better Clone Lens Cam!"
                                 MIMEType:@"image/jpeg"
                                arguments:[NSDictionary dictionaryWithObjectsAndKeys:@"1", @"is_public", nil]];
	
	[UIApplication sharedApplication].idleTimerDisabled = YES;
}

#pragma mark * OFFlickrAPIRequest delegate methods

- (void)flickrAPIRequest:(OFFlickrAPIRequest *)inRequest didObtainOAuthAccessToken:(NSString *)inAccessToken
                  secret:(NSString *)inSecret
            userFullName:(NSString *)inFullName
                userName:(NSString *)inUserName
                userNSID:(NSString *)inNSID
{
    [self setAndStoreFlickrAuthToken:inAccessToken secret:inSecret];
    self.flickrUserName = inUserName;
    [self flickrRequest].sessionInfo = nil;
    [self uploadImage:self.image];
}

- (void)flickrAPIRequest:(OFFlickrAPIRequest *)inRequest didCompleteWithResponse:(NSDictionary *)inResponseDictionary
{
    if (inRequest.sessionInfo == kCheckTokenStep) {
		self.flickrUserName = [inResponseDictionary valueForKeyPath:@"user.username._text"];
        return;
	}
    if (inRequest.sessionInfo == kUploadImageStep) {
        
        [ _delegate flickrManagerdidFilishUploadImage:nil ];
        NSString *photoID = [[inResponseDictionary valueForKeyPath:@"photoid"] textContent];
        
        _flickrRequest.sessionInfo = kSetImagePropertiesStep;
        [_flickrRequest callAPIMethodWithPOST:@"flickr.photos.setMeta" arguments:[NSDictionary dictionaryWithObjectsAndKeys:photoID, @"photo_id", @"Split Foto - better Clone Lens Cam!", @"title", @"Uploaded from my iPhone/iPod Touch", @"description", nil]];
        return;
	}
     if (inRequest.sessionInfo == kSetImagePropertiesStep) {
        [UIApplication sharedApplication].idleTimerDisabled = NO;
        [self flickrRequest].sessionInfo = nil;
        return;
    }
}

- (void)flickrAPIRequest:(OFFlickrAPIRequest *)inRequest didFailWithError:(NSError *)inError
{
	if (inRequest.sessionInfo == kGetAccessTokenStep)
    {
    }
	else if (inRequest.sessionInfo == kCheckTokenStep)
    {
		[self setAndStoreFlickrAuthToken:nil secret:nil];
	}

    [ _delegate flickrManagerdidFilishUploadImage:inError ];
    
}

- (void)flickrAPIRequest:(OFFlickrAPIRequest *)inRequest didObtainOAuthRequestToken:(NSString *)inRequestToken
                  secret:(NSString *)inSecret
{
    self.flickrContext.OAuthToken = inRequestToken;
    self.flickrContext.OAuthTokenSecret = inSecret;
    
    NSURL *authURL = [self.flickrContext userAuthorizationURLWithRequestToken:inRequestToken requestedPermission:OFFlickrWritePermission];
    [[UIApplication sharedApplication] openURL:authURL];
}

@end
