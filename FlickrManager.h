//
//  FlickrManager.h
//  InstaFrame Deluxe
//
//  Created by Roman Kopaliani on 7/27/12.
//  Copyright (c) 2012 FactorialComplexity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ObjectiveFlickr.h"

@class FlickrManager;

@protocol  FlickrManagerDelegate<NSObject>

-(void)flickrManagerdidFilishUploadImage:(NSError*)error;

@end

@interface FlickrManager : NSObject
{
    id <FlickrManagerDelegate> _delegate;
}

@property(nonatomic, retain)id<FlickrManagerDelegate> delegate;

+ (FlickrManager *)sharedManager;

- (void)uploadImage:(UIImage *)image;
- (BOOL)flickrHandleURL:(NSURL *)anURL;
- (BOOL)isFlickrLoggedIn;

@end
